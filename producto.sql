use prueba;

create table producto(
ID int not null auto_increment primary key,
NOMBRE varchar(30),
PRECIO int
);


insert into producto(NOMBRE,PRECIO) values ('Samsung Galaxy S8',15000);
insert into producto(NOMBRE,PRECIO) values ('LG G6',12000);
insert into producto(NOMBRE,PRECIO) values ('Xiaomi Mix2',18000);

create table detalleProducto(
ID int not null auto_increment primary key,
ID_PRODUCTO int not null,
DETALLE varchar(150),
foreign key(ID_PRODUCTO) references PRODUCTO(id)
);

insert into detalleProducto(ID_PRODUCTO,DETALLE) values (1,'pantalla super AMOLED, RAM 8G, 64GB Almacenamiento, 12 megapixeles de camara');

insert into detalleProducto(ID_PRODUCTO,DETALLE) values (1,'pantalla IPS LCD, RAM 4G, 32/64GB Almacenamiento, 13 megapixeles de camara');
	
insert into detalleProducto(ID_PRODUCTO,DETALLE) values (1,'pantalla IPS, RAM 6G, 64/128/256GB Almacenamiento, 12 megapixeles de camara');
