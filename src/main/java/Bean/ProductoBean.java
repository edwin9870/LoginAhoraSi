package Bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIData;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.FacesDataModel;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.persistence.internal.sessions.factories.model.log.LogConfig;

import Entity.Detalle;
import Entity.Producto;
import Entity.Usuario;

@ManagedBean
@RequestScoped
public class ProductoBean implements Serializable{

	private static final long serialVersionUID = 2579310604140771888L;
	
	EntityManagerFactory emf = Persistence.createEntityManagerFactory("java");
	EntityManager em = emf.createEntityManager();
	
	
	
	
	private int id;
	
	private String nombre;
	
	private int precio;
	
	private String detalleProducto;
	

	public String getDetalleProducto() {
		return detalleProducto;
	}


	public void setDetalleProducto(String lista) {
		this.detalleProducto = lista;
	}


	public String link(String nombre, int id) {
		this.nombre = nombre;
		Detalle d = em.createNamedQuery("DetalleProducto.obtenerId", Detalle.class)
				.setParameter("idProducto", id)
				.getSingleResult();
		detalleProducto = d.getDetalle();
		
		System.out.println("detalle del producto: "+ d.getDetalle());
		System.out.println("nombre producto: "+ nombre);
		//System.out.println("id producto: "+ id);
	return "detalleProducto.xhtml";
	}
	
	
	/*private String producto;
	
	public String getProducto() {
		Producto p = (Producto) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("producto");
		return p.getNombre();
	}
	public void setUsuario(String producto) {
		this.producto = producto;
	}
*/
	
	/*public String obtenerNombre(String nombre) {
		System.out.println("nombre del producto: "+nombre);
		return nombre;
		
	}*/
	public List<Producto> findAll(){
		List <Producto> listaProducto = new ArrayList();
		listaProducto = em.createNamedQuery("Producto.nombre", Producto.class).getResultList();
		System.out.println("detalleProducto de productos" + listaProducto);
		return listaProducto;
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	
}
