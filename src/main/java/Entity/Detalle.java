package Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "detalleproducto")
@NamedQueries({
	@NamedQuery(name = "DetalleProducto.buscarDetalles", query = "SELECT a FROM Detalle a"),
	@NamedQuery(name = "DetalleProducto.obtenerId", query = "SELECT b FROM Detalle b WHERE b.producto.id =:idProducto")
})
public class Detalle implements Serializable{

	private static final long serialVersionUID = -2372082949664647391L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private int id;
	
	@Column(name = "Detalle")
	private String detalle;
	
	@JoinColumn(name = "ID_PRODUCTO")
	@OneToOne
	private Producto producto;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return "Detalle [id=" + id + ", detalle=" + detalle + ", producto ]";
	}
	
	
	
	
}
